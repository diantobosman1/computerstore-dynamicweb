//--- This is the buy laptop application
class buyLaptop {
    constructor(selectLaptop){
    this.selectLaptop = selectLaptop;

    const totalBalanceElement = document.getElementById("totalBalance");
    const buyLaptopElement = document.getElementById("buyLaptop");

    //--- price of the first laptop
    this.selectLaptop.price = 200;

    //--- Method to handle the loan
    const handleBuyLaptop = () => {
        if (this.selectLaptop.price >= bApp.totalBalance) {
            window.alert("This is not possible because you do not have enough funds!")
        }
        else {
            //--- Calculate the new balance and set the new balance
            bApp.totalBalance = parseInt(bApp.totalBalance) - parseInt(this.selectLaptop.price);
            totalBalanceElement.innerText = "Total Balance: " + bApp.totalBalance + " EUR";
            window.alert("You are now the owner of the new laptop!")
        }
    }

    //--- When the button is clicked, the laptop should be purchased. 
    buyLaptopElement.addEventListener("click", handleBuyLaptop);
    }
}

//--- Initializes this class
const buyApp = new buyLaptop([]);






