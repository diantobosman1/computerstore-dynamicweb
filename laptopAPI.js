//--- For the laptopAPI (display of the different laptops with their features)

const laptopsElement = document.getElementById("laptops");
const specsElement = document.getElementById("specs");

let laptops = [];
let specs = []

//--- Fetch the data from the link
fetching("https://noroff-komputer-store-api.herokuapp.com/computers");

//--- Function for fetching
function fetching(URL) {
    return fetch (URL)
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToList(laptops));
}

//--- These are used to append the laptops
const addLaptopsToList = (laptops) => {
    laptops.forEach(x => addLaptopToList(x));
    specsElement.innerText = " \n Features: \n" + laptops[0].specs;
}

const addLaptopToList = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

//--- When another laptop is picked, the features etc also change
const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    specsElement.innerText = "\n Features: \n " + selectedLaptop.specs;
    
    //--- When the laptop changes these have to change too.
    switchPicture(selectedLaptop);
    switchTitle(selectedLaptop);
    switchDescription(selectedLaptop);
    switchPrice(selectedLaptop);
    buyApp.selectLaptop = selectedLaptop;
}

function switchPicture(selectedLaptop) {
    basisURL = "https://noroff-komputer-store-api.herokuapp.com/";
    extraURL = selectedLaptop.image;

    if (extraURL == "assets/images/5.jpg") {
        extraURL = "assets/images/5.png"
    }

    let laptopURL = basisURL.concat(extraURL);
    const myImageIdElement = document.getElementById("myImageId");
    myImageIdElement.src = laptopURL;

}

function switchTitle(selectedLaptop) {
    const nameOfTheLaptopElement = document.getElementById("nameOfTheLaptop");
    nameOfTheLaptopElement.innerText = selectedLaptop.title;
}

function switchDescription(selectedLaptop) {
    const descriptionLaptopElement = document.getElementById("descriptionLaptop");
    descriptionLaptopElement.innerText = selectedLaptop.description;
}

function switchPrice(selectedLaptop) {
    const laptopPriceElement = document.getElementById("laptopPrice");
    laptopPriceElement.innerText = selectedLaptop.price + " EUR";
}

//--- Launches the handleLaptopChange function when another laptop is picked
laptopsElement.addEventListener("change", handleLaptopChange);

