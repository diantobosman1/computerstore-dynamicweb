# ComputerStore DynamicWeb
This project is created as part of the Java Fullstack - Experis Remote course. 
This application is to show a dynamic webpage using vanilla javascript. There is a bank where one can store their money and also loan money. There is also a work section where on can create more money, which is then added to the bank. Also, there is a button to purchase a laptop.

## Getting Started
This project can be cloned using: 
git clone: https://gitlab.com/diantobosman1/computerstore-dynamicweb.git

### Prerequisites
None

## Built With
Visual Studio Code using javascript and html.

## Authors
Dianto Bosman
