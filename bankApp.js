//--- This is the bank application
class bankApp {
    constructor(totalBalance, totalLoan, alreadyBorrowed){
    
    //--- Get the elements out of the html file
    const totalBalanceElement = document.getElementById("totalBalance");
    const totalLoanElement = document.getElementById("totalLoan");
    const getLoanElement = document.getElementById("getLoan");

    //--- Fields (The alreadyBorrow one is a Boolean to trigger if there is a loan)
    this.totalBalance = totalBalance;
    this.totalLoan = totalLoan;
    this.alreadyBorrowed = alreadyBorrowed;

    //--- Method to handle the loan
    const handleLoan = () => {
        let loanPopup = prompt("How much would you like to borrow from the bank?")

        if (parseInt(loanPopup) >= (2 * this.totalBalance)) {
            window.alert("The amount that you want to loan is more than double your current balance.")
        }
        else if (this.alreadyBorrowed == true) {
            window.alert("You have already borrowed and you didn't pay back the loan yet.")
        }
        else {
            this.alreadyBorrowed = true;

            //--- Calculate the new loan and balance values
            this.totalLoan = parseInt(this.totalLoan) + parseInt(loanPopup);
            this.totalBalance = parseInt(this.totalBalance) + parseInt(loanPopup);
            console.log("The loan is accepted by the bank and added to the balance.")
        }
        
        //--- Set new Balance and loan amount
        totalLoanElement.innerText = "Total loan: " + this.totalLoan + " EUR";
        totalBalanceElement.innerText = "Total Balance: " + this.totalBalance + " EUR";

    }
    
    //--- Plays the above method when one clicks on the get loan button
    getLoanElement.addEventListener("click", handleLoan);
    
    }    
}

//--- Launch the class with initial values (totalBalance, totalLoan)
const bApp = new bankApp(500, 0, new Boolean(false));