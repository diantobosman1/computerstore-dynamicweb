//--- WorkApp Class
class workApp {
    constructor(salaryTotalAmount, workSalary) {

   //--- Get the elements out of the html file
    const payWithBankElement = document.getElementById("payWithBank");
    const payWithWorkElement = document.getElementById("payWithWork");
    const salaryAmountElement = document.getElementById("salaryAmount");
    const totalBalanceElement = document.getElementById("totalBalance");
    const totalLoanElement = document.getElementById("totalLoan");
    const payLoanElement = document.getElementById("payLoan");

    //--- Fields
    this.salaryTotalAmount = salaryTotalAmount;
    this.workSalary = workSalary;
    this.totalLoan = totalLoan;
    
    //--- Method to handle the bank payment
    const handleBank = () => {

        //--- Check if there is a outstanding loan 
        if (bApp.alreadyBorrowed == true) {
            //--- Set 90% to the bank
            bApp.totalBalance = parseInt(bApp.totalBalance) + 0.9 * parseInt(this.salaryTotalAmount);

            //--- Calculate the remaining 10%, set in euros and set the new total loan price
            bApp.totalLoan = parseInt(bApp.totalLoan) - 0.1 * parseInt(this.salaryTotalAmount);
            totalLoanElement.innerText = "Total loan: " + bApp.totalLoan + " EUR";
            console.log("There is an outstanding loan. So 90% will go to the balance and 10% to the outstanding loan.")
        }

        else {
            bApp.totalBalance = parseInt(bApp.totalBalance) + parseInt(this.salaryTotalAmount);
            console.log("Salary added to the bank balance.");
        }
        
        this.salaryTotalAmount = 0;

        //--- Set new Balance and Salary Amount
        totalBalanceElement.innerText = "Total balance: " + + bApp.totalBalance + " EUR";
        salaryAmountElement.innerText = "Salary balance: " + this.salaryTotalAmount + " EUR";

    }

    //--- Method to handle the bank payment
    const handleWork = () => {
        this.salaryTotalAmount = parseInt(this.salaryTotalAmount) + parseInt(this.workSalary);
        salaryAmountElement.innerText = "Salary balance: " + this.salaryTotalAmount + " EUR";
        console.log("The salary is added to the bank.")

    }

    //--- Method to handle the payloan
    const handlepayLoan = () => {
        if (bApp.totalLoan > this.salaryTotalAmount) {
            bApp.totalLoan = parseInt(bApp.totalLoan) - parseInt(this.salaryTotalAmount);
            this.salaryTotalAmount = 0

            //--- Set new loan amount and Salary Amount
            totalLoanElement.innerText = "Total loan: " + bApp.totalLoan + " EUR";
            salaryAmountElement.innerText = "Salary balance: " + this.salaryTotalAmount + " EUR";
            console.log("A part of the loan is payed now.")
        }
        
        else {
            bApp.totalBalance =  parseInt(bApp.totalBalance) + ( parseInt(this.salaryTotalAmount)  -  parseInt(bApp.totalLoan))
            bApp.totalLoan = 0
            this.salaryTotalAmount = 0
            
            //--- Set new salary, loan and balance amount
            salaryAmountElement.innerText = "Salary balance: " + this.salaryTotalAmount + " EUR";
            totalLoanElement.innerText = "Total loan: " + bApp.totalLoan + " EUR";
            totalBalanceElement.innerText = "Total balance: " + " " + bApp.totalBalance + " EUR";
            console.log("The loan is paid completely!")
        }

    }

    //--- Plays the above method when one clicks on the get loan button
    payWithBankElement.addEventListener("click", handleBank);
    payWithWorkElement.addEventListener("click", handleWork);
    payLoanElement.addEventListener("click", handlepayLoan);

    }
}

//--- Launch the class with initial values (salaryTotalAmount, workSalary)
new workApp(0, 100);

